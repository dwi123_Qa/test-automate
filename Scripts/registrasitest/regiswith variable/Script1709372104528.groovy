import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://demowebshop.tricentis.com/register')

WebUI.click(findTestObject('Registerwebshop/Page_Register/input_Gender_Gender'))

WebUI.setText(findTestObject('Registerwebshop/Page_Register/input_First name'), Name)

WebUI.setText(findTestObject('Registerwebshop/Page_Register/input_Last name'), lastname)

WebUI.setText(findTestObject('Registerwebshop/Page_Register/input_Email'), Username)

WebUI.setText(findTestObject('Registerwebshop/Page_Register/input_Password'), Password)

WebUI.setText(findTestObject('Registerwebshop/Page_Register/input_Confirm password'), password)

WebUI.click(findTestObject('Registerwebshop/Page_Register/input__register-button'))

WebUI.waitForElementPresent(findTestObject('AppoinmentConfirmation/Registerresult/h1_Register'), 0)

WebUI.click(findTestObject('AppoinmentConfirmation/Registerresult/registration completed_button'))

WebUI.closeBrowser()

