import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('reusable-test/block-login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Keranjangbeli/Page_addcard/a_Computers'))

WebUI.click(findTestObject('Keranjangbeli/Page_addcard/Page_Notebook/a_Notebooks'))

WebUI.click(findTestObject('Keranjangbeli/Page_addcard/add/btn_addcard'))

WebUI.click(findTestObject('Keranjangbeli/Page_Remove/shooping_cart'))

WebUI.selectOptionByValue(findTestObject('Keranjangbeli/Page_Demo Web Shop. Shopping Cart/select_country'), '42', false)

WebUI.selectOptionByValue(findTestObject('Keranjangbeli/Page_Demo Web Shop. Shopping Cart/select_Other (Non US)'), '0', 
    false)

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Shopping Cart/btn_agree'))

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Shopping Cart/btn_Checkout'))

WebUI.waitForElementPresent(findTestObject('Keranjangbeli/Page_Demo Web Shop. Checkout/div_Select billing address'), 0)

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Checkout/btn_stepone'))

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Checkout/input_Fax number_PickUpInStore'))

WebUI.waitForElementPresent(findTestObject('Keranjangbeli/Page_Demo Web Shop. Checkout/div_SelectNew'), 0)

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Checkout/btn_steptwo'))

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Checkout/input_Payment'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Checkout/btn_steptree'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Checkout/btn_stepfour'))

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Checkout/btn_stepfive'))

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Checkout/a_Click here for order details'))

WebUI.click(findTestObject('Keranjangbeli/Page_Demo Web Shop. Order Details/a_Print'))


WebUI.closeBrowser()