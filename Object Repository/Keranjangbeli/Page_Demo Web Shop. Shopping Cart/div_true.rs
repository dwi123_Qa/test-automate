<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_true</name>
   <tag></tag>
   <elementGuidId>dc241fd3-9256-45d5-96cb-a005f7711dfe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.checkout-buttons</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='(read)'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>513faf8f-ea1a-49d8-8ba3-389bf584491e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkout-buttons</value>
      <webElementGuid>ecfb7a3b-a5e7-43a3-aef1-3797ab18eb08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                $(document).ready(function () {
                                    $('#checkout').click(function () {
                                        //terms of service
                                        var termOfServiceOk = true;
                                        if ($('#termsofservice').length > 0) {
                                            //terms of service element exists
                                            if (!$('#termsofservice').is(':checked')) {
                                                $(&quot;#terms-of-service-warning-box&quot;).dialog();
                                                termOfServiceOk = false;
                                            } else {
                                                termOfServiceOk = true;
                                            }
                                        }
                                        return termOfServiceOk;
                                    });
                                });
                            
                            
                                Checkout
                            
                    </value>
      <webElementGuid>bab58af0-7557-4330-84e7-a688475ba371</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;totals&quot;]/div[@class=&quot;checkout-buttons&quot;]</value>
      <webElementGuid>c6ddcc09-8ecd-45f2-8578-18af56113f94</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(read)'])[1]/following::div[1]</value>
      <webElementGuid>adefcf37-de8e-42b1-bb99-fc1486b3acf8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::div[2]</value>
      <webElementGuid>a503bdba-49e0-4967-bbe4-9c4b9779f1de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[4]</value>
      <webElementGuid>ffd24e37-3dad-4b97-9d76-aa55fb572fe5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
                            
                                $(document).ready(function () {
                                    $(&quot; , &quot;'&quot; , &quot;#checkout&quot; , &quot;'&quot; , &quot;).click(function () {
                                        //terms of service
                                        var termOfServiceOk = true;
                                        if ($(&quot; , &quot;'&quot; , &quot;#termsofservice&quot; , &quot;'&quot; , &quot;).length > 0) {
                                            //terms of service element exists
                                            if (!$(&quot; , &quot;'&quot; , &quot;#termsofservice&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:checked&quot; , &quot;'&quot; , &quot;)) {
                                                $(&quot;#terms-of-service-warning-box&quot;).dialog();
                                                termOfServiceOk = false;
                                            } else {
                                                termOfServiceOk = true;
                                            }
                                        }
                                        return termOfServiceOk;
                                    });
                                });
                            
                            
                                Checkout
                            
                    &quot;) or . = concat(&quot;
                            
                                $(document).ready(function () {
                                    $(&quot; , &quot;'&quot; , &quot;#checkout&quot; , &quot;'&quot; , &quot;).click(function () {
                                        //terms of service
                                        var termOfServiceOk = true;
                                        if ($(&quot; , &quot;'&quot; , &quot;#termsofservice&quot; , &quot;'&quot; , &quot;).length > 0) {
                                            //terms of service element exists
                                            if (!$(&quot; , &quot;'&quot; , &quot;#termsofservice&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:checked&quot; , &quot;'&quot; , &quot;)) {
                                                $(&quot;#terms-of-service-warning-box&quot;).dialog();
                                                termOfServiceOk = false;
                                            } else {
                                                termOfServiceOk = true;
                                            }
                                        }
                                        return termOfServiceOk;
                                    });
                                });
                            
                            
                                Checkout
                            
                    &quot;))]</value>
      <webElementGuid>8958e162-e4d1-4666-a7dc-106f9c497e51</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
