<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_agree</name>
   <tag></tag>
   <elementGuidId>7db4bf10-93bd-4ba4-bd27-037ee3e00206</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.terms-of-service</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Total:'])[2]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f4b5fd90-3ecb-46f0-8cbe-8c9e78c2cb6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>terms-of-service</value>
      <webElementGuid>78a787ed-4d25-4549-bc97-f9842de70d1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            I agree with the terms of service and I adhere to them unconditionally
                            (read)
                        </value>
      <webElementGuid>b66dead7-cc9a-4241-b198-94d88d56fffe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;totals&quot;]/div[@class=&quot;terms-of-service&quot;]</value>
      <webElementGuid>bac9249a-112f-429c-ae36-872a41fb18cb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total:'])[2]/following::div[2]</value>
      <webElementGuid>3a05faed-5a3e-4089-af3d-015cd499aa96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tax:'])[1]/following::div[2]</value>
      <webElementGuid>97c95856-4843-4039-8f59-cd4b7cdda74f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[1]/preceding::div[1]</value>
      <webElementGuid>daa76bda-29f6-467c-a701-90fbe8578448</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='I agree with the terms of service and I adhere to them unconditionally']/parent::*</value>
      <webElementGuid>a8182dd9-91ef-4a96-96b5-2e801d8b87f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[3]</value>
      <webElementGuid>bf4bb7c2-c82f-44ad-a3fd-b27fe549b4d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                            I agree with the terms of service and I adhere to them unconditionally
                            (read)
                        ' or . = '
                            
                            I agree with the terms of service and I adhere to them unconditionally
                            (read)
                        ')]</value>
      <webElementGuid>5847cb03-b8f1-4260-8ddf-dcd4d3a93c48</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
