<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Print</name>
   <tag></tag>
   <elementGuidId>0d67538f-186f-4dca-82e1-a1b5e4c250bb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.button-2.print-order-button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Print')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>86c79b6b-62f5-4873-89a8-7c0f5a8165a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/orderdetails/print/1623764</value>
      <webElementGuid>620df618-e09f-4d82-b1bb-06d70eb735c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
      <webElementGuid>f28913b8-245f-4425-92af-aff446816983</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-2 print-order-button</value>
      <webElementGuid>0f4cc825-da5a-4eb1-bcfc-fc024bd80fcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Print</value>
      <webElementGuid>2e82ff3d-8428-4756-9ea2-458bbabb511a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page order-details-page&quot;]/div[@class=&quot;page-title&quot;]/a[@class=&quot;button-2 print-order-button&quot;]</value>
      <webElementGuid>44a15116-5f8d-49a7-9c7c-1865d4ca57f6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Print')]</value>
      <webElementGuid>47239273-799f-4efa-9f20-4655912e191e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order information'])[1]/following::a[1]</value>
      <webElementGuid>d80c6e97-ff5e-446c-9549-015a9b081a3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::a[1]</value>
      <webElementGuid>2e0855c9-efb2-483a-b21a-66e608679681</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PDF Invoice'])[1]/preceding::a[1]</value>
      <webElementGuid>dbcb9c88-ad28-43c1-828e-fa85ee77284c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order #1623764'])[1]/preceding::a[2]</value>
      <webElementGuid>d71ed1d4-3176-479f-aac1-b043117ac180</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Print']/parent::*</value>
      <webElementGuid>8b31d541-f646-420b-9bbf-a2b09410bb78</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/orderdetails/print/1623764')]</value>
      <webElementGuid>47d28b8e-8225-4b6d-b2d2-5da6a208fec1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/div/div/div/a</value>
      <webElementGuid>49b5c623-ea41-43d0-bdbb-2958b63ba67e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/orderdetails/print/1623764' and (text() = 'Print' or . = 'Print')]</value>
      <webElementGuid>1477885a-94c8-4f2f-9b33-749cbd3623aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
