<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_card empty</name>
   <tag></tag>
   <elementGuidId>9e7cb59d-0fe5-41d6-9b9c-e6c69a509e9d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.page.shopping-cart-page</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Complete'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>25e19284-b41b-4708-aeb3-125474a50930</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page shopping-cart-page</value>
      <webElementGuid>8986ad23-5754-48da-8c9c-80e72b60981e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        Shopping cart
    
    
        

    
    
Your Shopping Cart is empty!    


    
</value>
      <webElementGuid>1898dac1-b8b9-4e26-aa1e-12b7e51ee1db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]</value>
      <webElementGuid>da81315f-4b2f-4239-b70d-8c3be660abd4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Complete'])[1]/following::div[1]</value>
      <webElementGuid>b8e20303-de59-4610-8e31-e8184f15cb67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm'])[1]/following::div[1]</value>
      <webElementGuid>782fac1c-5b81-4958-b27b-aca717babf24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/div/div[2]</value>
      <webElementGuid>34e35cbc-43ed-4cdf-8867-192bd81a108f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        Shopping cart
    
    
        

    
    
Your Shopping Cart is empty!    


    
' or . = '
    
        Shopping cart
    
    
        

    
    
Your Shopping Cart is empty!    


    
')]</value>
      <webElementGuid>229f08b3-46f1-4755-8a98-42c83150c376</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
