<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Check (5.00)</name>
   <tag></tag>
   <elementGuidId>7035a0aa-8deb-42a0-8ddc-867a9929ae0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-method-load']/div/div/ul/li[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>db68ad7c-0f12-40ca-90dd-1dca87c8b842</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>method-name</value>
      <webElementGuid>d270fde1-4388-4450-94ac-20c4e2804315</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    
                                        
                                    
                                
                            
                                
                                Check / Money Order (5.00)
                            
                        </value>
      <webElementGuid>22acf5ed-c3af-4016-8f72-429e5cb139cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-method-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-method&quot;]/ul[@class=&quot;method-list&quot;]/li[2]/div[@class=&quot;method-name&quot;]</value>
      <webElementGuid>6b712782-e7d2-46ce-bd79-fa92c9a68e87</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-method-load']/div/div/ul/li[2]/div</value>
      <webElementGuid>c0246c6c-d259-4fa0-a4bc-f028f0ff98e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cash On Delivery (COD) (7.00)'])[1]/following::div[1]</value>
      <webElementGuid>3865b234-20ed-4a95-b094-f64cc19fdb91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment method'])[1]/following::div[8]</value>
      <webElementGuid>489506e8-af30-4645-82cf-9b292888ad58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Credit Card'])[1]/preceding::div[4]</value>
      <webElementGuid>55f5ae4d-a992-49e6-b173-7d1e7792e012</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/ul/li[2]/div</value>
      <webElementGuid>baebc830-4d14-4aac-a022-c17676541aeb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    
                                        
                                    
                                
                            
                                
                                Check / Money Order (5.00)
                            
                        ' or . = '
                                
                                    
                                        
                                    
                                
                            
                                
                                Check / Money Order (5.00)
                            
                        ')]</value>
      <webElementGuid>a9d36468-95e9-4dee-8608-9c3de4066f72</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
