<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_COD (1)</name>
   <tag></tag>
   <elementGuidId>aa50ee06-f43a-4aa1-9a89-b8301cccdeaf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.method-name</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-method-load']/div/div/ul/li/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2ba4cde3-ec5f-4982-a55b-03b62b7e0618</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>method-name</value>
      <webElementGuid>f688478f-43a3-4c31-b3a8-57cc674651a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    
                                        
                                    
                                
                            
                                
                                Cash On Delivery (COD) (7.00)
                            
                        </value>
      <webElementGuid>f78a6d1f-5921-4bc4-bbd8-2b94baa5e4ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-method-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-method&quot;]/ul[@class=&quot;method-list&quot;]/li[1]/div[@class=&quot;method-name&quot;]</value>
      <webElementGuid>e907aeb6-f374-4191-8354-7c19cb56805f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-method-load']/div/div/ul/li/div</value>
      <webElementGuid>1d52f5cc-bdb6-47bf-af54-29b3a1c575cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment method'])[1]/following::div[5]</value>
      <webElementGuid>ab3ad906-8b01-4bf3-ba55-835dc33a0c55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[3]/following::div[6]</value>
      <webElementGuid>a75a06de-d165-49cb-b903-07e75220c3bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Check / Money Order (5.00)'])[1]/preceding::div[4]</value>
      <webElementGuid>b5b1dae2-5809-431c-989b-63086a2e674d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/ul/li/div</value>
      <webElementGuid>0a6317e6-d2af-4b3b-b6d6-f2ac20abc419</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    
                                        
                                    
                                
                            
                                
                                Cash On Delivery (COD) (7.00)
                            
                        ' or . = '
                                
                                    
                                        
                                    
                                
                            
                                
                                Cash On Delivery (COD) (7.00)
                            
                        ')]</value>
      <webElementGuid>c7d4989f-19de-4bc5-8a9f-075349fc287a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
