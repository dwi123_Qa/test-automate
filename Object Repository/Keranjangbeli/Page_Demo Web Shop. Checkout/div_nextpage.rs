<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_nextpage</name>
   <tag></tag>
   <elementGuidId>05865af0-8ef2-4ba6-b698-a01fa97bb451</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#shipping-buttons-container</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='shipping-buttons-container']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>28764d52-529e-45dd-a308-8580ca177293</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>buttons</value>
      <webElementGuid>a1aea192-6715-499d-9f07-d4bcc83d4da0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>shipping-buttons-container</value>
      <webElementGuid>7545eb14-0a78-41a9-bee9-a91979270799</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                « Back
                            
                             Loading next step...
                        </value>
      <webElementGuid>03392f1c-ae6b-413c-97a3-2eabac4b2a18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shipping-buttons-container&quot;)</value>
      <webElementGuid>c187be87-4fd3-48bf-9b4b-da4888d8c392</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='shipping-buttons-container']</value>
      <webElementGuid>cc4d7424-5d23-4e7c-b6f7-d7b5d4358f50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-step-shipping']/div</value>
      <webElementGuid>ee397499-6014-4622-8a4b-58e9b35e2f80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pick up your items at the store (put your store address here)'])[1]/following::div[1]</value>
      <webElementGuid>27b1bb76-338a-4aef-b725-cbc4164ac7cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In-Store Pickup'])[1]/following::div[1]</value>
      <webElementGuid>a86c4624-412d-4fdf-a3f3-afd1777b034b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/div[2]/div</value>
      <webElementGuid>a0a0c79d-d34b-4420-8e40-dcafad70a4c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'shipping-buttons-container' and (text() = '
                            
                                « Back
                            
                             Loading next step...
                        ' or . = '
                            
                                « Back
                            
                             Loading next step...
                        ')]</value>
      <webElementGuid>f4c0d836-5f4f-4be7-8ed2-1e4ab10bde85</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
