<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_In-Store Pickup</name>
   <tag></tag>
   <elementGuidId>87923876-a00d-4692-bd18-a3c78fe22dab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.selector</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-shipping-load']/div/div[2]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>93520418-5302-4606-8ba2-8093d3a71277</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>selector</value>
      <webElementGuid>d68021ab-7342-4458-93d0-34e39efc2a6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                In-Store Pickup
            </value>
      <webElementGuid>03744467-0395-4ed0-8f25-4b96057cda88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-shipping-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section pickup-in-store&quot;]/p[@class=&quot;selector&quot;]</value>
      <webElementGuid>9e4a5d4f-45b4-4804-96ae-bc8e1e7819ed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-shipping-load']/div/div[2]/p</value>
      <webElementGuid>6a4f3803-e200-4fa8-a045-1b631f17b997</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fax number:'])[2]/following::p[1]</value>
      <webElementGuid>4f0a727a-b927-45f7-b5eb-411d22acad45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[16]/following::p[1]</value>
      <webElementGuid>5f359b34-b010-41c2-ad49-3f358411d674</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pick up your items at the store (put your store address here)'])[1]/preceding::p[1]</value>
      <webElementGuid>9b6a087f-2d06-4a7f-9936-5f4a148f60ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>79f400f9-084c-42fa-bcba-b356eb676305</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '
                
                In-Store Pickup
            ' or . = '
                
                In-Store Pickup
            ')]</value>
      <webElementGuid>3717bcc8-d792-4e94-8183-15669bc15c5b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
