<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_loadingtre</name>
   <tag></tag>
   <elementGuidId>80aeb438-2560-4c2c-909a-1a0371c311ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#payment-info-buttons-container</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='payment-info-buttons-container']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>60ebf142-e162-443f-874b-88fb7ca8e0b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>buttons</value>
      <webElementGuid>0ff2660b-e327-46ae-89d8-676e411af30f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>payment-info-buttons-container</value>
      <webElementGuid>bd34bee0-829d-4a81-9588-335feb5358b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            « Back
                        
                        Loading next step...
                    </value>
      <webElementGuid>01ce942a-2e72-4108-8552-2da5a6eabbae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;payment-info-buttons-container&quot;)</value>
      <webElementGuid>ff223507-047c-489b-8780-36b74cfebe3f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='payment-info-buttons-container']</value>
      <webElementGuid>e7893569-ba34-4361-8532-3482e3685cca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-step-payment-info']/div</value>
      <webElementGuid>902a0344-0ccd-4a59-a651-7896e800d862</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment information'])[1]/following::div[8]</value>
      <webElementGuid>13a78415-9553-42cd-adea-d3bc4526cd99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[4]/following::div[9]</value>
      <webElementGuid>3741c869-78a4-4e6a-ac86-4dd81b582bcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div[2]/div</value>
      <webElementGuid>5019452d-7f3c-4bdf-9738-8185898b99ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'payment-info-buttons-container' and (text() = '
                        
                            « Back
                        
                        Loading next step...
                    ' or . = '
                        
                            « Back
                        
                        Loading next step...
                    ')]</value>
      <webElementGuid>2cf1210d-2189-42b9-8546-518b29c7b145</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
