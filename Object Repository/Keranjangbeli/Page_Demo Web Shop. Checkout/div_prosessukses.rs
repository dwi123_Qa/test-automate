<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_prosessukses</name>
   <tag></tag>
   <elementGuidId>07fe5916-2fa4-471f-a696-d1bd5a7f1423</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.section.order-completed</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Thank you'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>bd3cb2ec-6705-48fb-a9a6-5579b07e0c6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>section order-completed</value>
      <webElementGuid>d0f874d5-ef07-44a2-9f6d-471501d3f523</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                Your order has been successfully processed!
            
            
                
                    Order number: 1623545
                
                
                    Click here for order details.
                
            
            
                
            
        </value>
      <webElementGuid>e9f9e2d2-aafa-4324-9234-c5d49cee4f91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page checkout-page&quot;]/div[@class=&quot;page-body checkout-data&quot;]/div[@class=&quot;section order-completed&quot;]</value>
      <webElementGuid>0bc9765e-1bc3-4a65-b62d-dd482ef0249f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Thank you'])[1]/following::div[2]</value>
      <webElementGuid>dff2fc24-f494-4669-bff8-0204f91e7a85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::div[9]</value>
      <webElementGuid>8e69b212-91f4-4317-bf8a-85c5172a5c8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/div/div/div[2]/div</value>
      <webElementGuid>6ee348b4-c275-4a5a-ab5d-312ba8280aa0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
                Your order has been successfully processed!
            
            
                
                    Order number: 1623545
                
                
                    Click here for order details.
                
            
            
                
            
        ' or . = '
            
                Your order has been successfully processed!
            
            
                
                    Order number: 1623545
                
                
                    Click here for order details.
                
            
            
                
            
        ')]</value>
      <webElementGuid>b713fcd3-f0da-409e-8edd-e5d5b4512f7a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
