<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_payCOD</name>
   <tag></tag>
   <elementGuidId>fb2aaf0a-d246-4f07-9c9c-2c5b6709c7ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#checkout-step-payment-info</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-step-payment-info']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5b1be225-32ee-4f98-af0b-708da8ac0c74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkout-step-payment-info</value>
      <webElementGuid>31eab324-2808-4c0b-a823-11fa3b2cfc7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>step a-item</value>
      <webElementGuid>d7f9f35b-27fb-4490-9eba-7b15927d2e86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
    
    
        
            

    
        
            You will pay by COD
        
    


        
        
            

        
    
    


                    
                    
                        PaymentInfo.init('#co-payment-info-form', 'https://demowebshop.tricentis.com/checkout/OpcSavePaymentInfo/');
                    
                    
                        
                            « Back
                        
                        Loading next step...
                    
                </value>
      <webElementGuid>9b28d015-048a-434c-bc64-7ad3894bc6b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-step-payment-info&quot;)</value>
      <webElementGuid>5565e7c2-da79-4b00-abf3-7cc79137257b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='checkout-step-payment-info']</value>
      <webElementGuid>fa91ad5f-5a60-40b7-97f8-f6fa4b112c7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='opc-payment_info']/div[2]</value>
      <webElementGuid>4ae120b5-30bc-49be-a812-388df0d72465</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment information'])[1]/following::div[1]</value>
      <webElementGuid>d99662a4-09e1-49db-b724-e7486bf7d7f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[4]/following::div[2]</value>
      <webElementGuid>80a17ffe-d9fe-47b9-8f4f-74c398294266</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div[2]</value>
      <webElementGuid>28b0755b-ef21-4e19-9894-139821ba33a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'checkout-step-payment-info' and (text() = concat(&quot;
                    
                    
    
    
        
            

    
        
            You will pay by COD
        
    


        
        
            

        
    
    


                    
                    
                        PaymentInfo.init(&quot; , &quot;'&quot; , &quot;#co-payment-info-form&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/OpcSavePaymentInfo/&quot; , &quot;'&quot; , &quot;);
                    
                    
                        
                            « Back
                        
                        Loading next step...
                    
                &quot;) or . = concat(&quot;
                    
                    
    
    
        
            

    
        
            You will pay by COD
        
    


        
        
            

        
    
    


                    
                    
                        PaymentInfo.init(&quot; , &quot;'&quot; , &quot;#co-payment-info-form&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/OpcSavePaymentInfo/&quot; , &quot;'&quot; , &quot;);
                    
                    
                        
                            « Back
                        
                        Loading next step...
                    
                &quot;))]</value>
      <webElementGuid>2e9397ce-526c-4502-a09b-c619958e529f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
