<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Credit Card</name>
   <tag></tag>
   <elementGuidId>9547f690-bf0e-4f1d-8059-1044e04ae223</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-method-load']/div/div/ul/li[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9c141933-e75b-4481-a31e-2c43ad2621a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>method-name</value>
      <webElementGuid>c759af3b-9e7b-4857-9d7e-2ce56494baf4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    
                                        
                                    
                                
                            
                                
                                Credit Card
                            
                        </value>
      <webElementGuid>d6cb76da-5e32-415b-9458-0295023bcdb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-method-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-method&quot;]/ul[@class=&quot;method-list&quot;]/li[3]/div[@class=&quot;method-name&quot;]</value>
      <webElementGuid>f7b1434d-f6ba-444b-ba6d-82eedb28e422</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-method-load']/div/div/ul/li[3]/div</value>
      <webElementGuid>3ac6db93-4647-489a-9007-a91dc54ad53c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Check / Money Order (5.00)'])[1]/following::div[1]</value>
      <webElementGuid>6a4b3617-a866-4614-bbb7-1bf424297168</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cash On Delivery (COD) (7.00)'])[1]/following::div[4]</value>
      <webElementGuid>fbc79cd1-5d3b-4293-8cdd-9376c5760fb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Purchase Order'])[1]/preceding::div[4]</value>
      <webElementGuid>531c4be1-6a72-451f-ae53-8e9dc3e5be9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/ul/li[3]/div</value>
      <webElementGuid>5eb6aee8-b3f7-4d2c-b3b0-34eaf72a25b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    
                                        
                                    
                                
                            
                                
                                Credit Card
                            
                        ' or . = '
                                
                                    
                                        
                                    
                                
                            
                                
                                Credit Card
                            
                        ')]</value>
      <webElementGuid>4c97c840-1cc3-4125-a2a1-d0c4bfb111fb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
