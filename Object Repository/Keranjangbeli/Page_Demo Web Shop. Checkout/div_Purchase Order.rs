<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Purchase Order</name>
   <tag></tag>
   <elementGuidId>f29b983b-b057-4c04-80f1-23e5d23a45f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-method-load']/div/div/ul/li[4]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7ed72d91-8fd9-4c84-8537-6b4a99642556</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>method-name</value>
      <webElementGuid>ec47fcb3-6cad-46e5-944e-68af243f5255</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    
                                        
                                    
                                
                            
                                
                                Purchase Order
                            
                        </value>
      <webElementGuid>8f1f06ab-010c-4a66-afad-0369a0296def</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-method-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-method&quot;]/ul[@class=&quot;method-list&quot;]/li[4]/div[@class=&quot;method-name&quot;]</value>
      <webElementGuid>78338f62-3afc-4f55-a64a-2fab74c5ff8a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-method-load']/div/div/ul/li[4]/div</value>
      <webElementGuid>a1130659-7868-4aa4-95ac-80b9d7ca81e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Credit Card'])[1]/following::div[1]</value>
      <webElementGuid>dc1bb98c-fd42-4c63-b3a2-d857140d8d21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Check / Money Order (5.00)'])[1]/following::div[4]</value>
      <webElementGuid>8165b4aa-3e64-4070-8166-45563a718c61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[3]/preceding::div[3]</value>
      <webElementGuid>ab2952ee-5a7a-4db5-9f0f-9fa5be97cbff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/ul/li[4]/div</value>
      <webElementGuid>db367d10-2737-4c55-b26d-1d2bfe4b9399</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    
                                        
                                    
                                
                            
                                
                                Purchase Order
                            
                        ' or . = '
                                
                                    
                                        
                                    
                                
                            
                                
                                Purchase Order
                            
                        ')]</value>
      <webElementGuid>a0160b83-3b72-4f16-80c1-d1782f58f64b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
