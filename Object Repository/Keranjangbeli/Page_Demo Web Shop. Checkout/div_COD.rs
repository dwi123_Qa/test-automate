<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_COD</name>
   <tag></tag>
   <elementGuidId>5db9cd7a-5813-4b71-b231-4431d9ee9322</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#checkout-step-payment-method</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-step-payment-method']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ae902b86-1e18-4cc9-855f-efb9a933448b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkout-step-payment-method</value>
      <webElementGuid>53e08e7e-25d9-43aa-8509-c54770f05834</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>step a-item</value>
      <webElementGuid>70ab17b4-550a-4a39-830d-ec8547384c36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
    
    
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Cash On Delivery (COD) (7.00)
                            
                        
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Check / Money Order (5.00)
                            
                        
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Credit Card
                            
                        
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Purchase Order
                            
                        
                    
            
    
    


                    
                    
                        PaymentMethod.init('#co-payment-method-form', 'https://demowebshop.tricentis.com/checkout/OpcSavePaymentMethod/');
                    
                    
                        
                            « Back
                        
                        Loading next step...
                    
                </value>
      <webElementGuid>0ae6b0fd-fb39-48e7-8b35-7e3d3000a593</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-step-payment-method&quot;)</value>
      <webElementGuid>9d095a48-0526-4e18-9eb8-f8ea2a5d9d7a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='checkout-step-payment-method']</value>
      <webElementGuid>29a89e26-0c61-40cb-a923-698e7b243678</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='opc-payment_method']/div[2]</value>
      <webElementGuid>d0175705-48b1-42a5-a9c8-bfe70bdb82cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment method'])[1]/following::div[1]</value>
      <webElementGuid>255f5026-4333-4bfd-ae42-ec2be405f632</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[3]/following::div[2]</value>
      <webElementGuid>2cca1e37-c3c3-4e74-8e16-41ff8d740c2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/div[2]</value>
      <webElementGuid>ba85ff19-f5b6-465a-822c-4fa9b05a379d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'checkout-step-payment-method' and (text() = concat(&quot;
                    
                    
    
    
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Cash On Delivery (COD) (7.00)
                            
                        
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Check / Money Order (5.00)
                            
                        
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Credit Card
                            
                        
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Purchase Order
                            
                        
                    
            
    
    


                    
                    
                        PaymentMethod.init(&quot; , &quot;'&quot; , &quot;#co-payment-method-form&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/OpcSavePaymentMethod/&quot; , &quot;'&quot; , &quot;);
                    
                    
                        
                            « Back
                        
                        Loading next step...
                    
                &quot;) or . = concat(&quot;
                    
                    
    
    
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Cash On Delivery (COD) (7.00)
                            
                        
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Check / Money Order (5.00)
                            
                        
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Credit Card
                            
                        
                    
                    
                        
                                
                                    
                                        
                                    
                                
                            
                                
                                Purchase Order
                            
                        
                    
            
    
    


                    
                    
                        PaymentMethod.init(&quot; , &quot;'&quot; , &quot;#co-payment-method-form&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/OpcSavePaymentMethod/&quot; , &quot;'&quot; , &quot;);
                    
                    
                        
                            « Back
                        
                        Loading next step...
                    
                &quot;))]</value>
      <webElementGuid>87f22823-def7-4f31-bdbd-0f945f92f77a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
