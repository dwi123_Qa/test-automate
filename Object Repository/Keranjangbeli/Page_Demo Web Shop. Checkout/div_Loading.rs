<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Loading</name>
   <tag></tag>
   <elementGuidId>e2abb41e-261b-4a3f-a6c6-a2649b555621</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#billing-buttons-container</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='billing-buttons-container']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e22bdeee-c72a-457f-a692-e72780d1681d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>buttons</value>
      <webElementGuid>fdf16363-0e4d-4a6d-b051-b59f41156436</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>billing-buttons-container</value>
      <webElementGuid>b42e7dbc-98fe-4217-a563-1e03f220d68c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Loading next step...
                    </value>
      <webElementGuid>27fb0d12-a50c-4f89-ad90-e7f9b399bac1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;billing-buttons-container&quot;)</value>
      <webElementGuid>a6162e2f-b829-421b-ad9c-e72466f58d56</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='billing-buttons-container']</value>
      <webElementGuid>77e90924-fc1e-4a7d-8c3f-6e635e200a7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-step-billing']/div</value>
      <webElementGuid>9e7564bc-aa24-40f5-ae56-38c434d7373c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fax number:'])[1]/following::div[1]</value>
      <webElementGuid>be399c06-7b18-47b8-bb56-1cb397220011</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[8]/following::div[2]</value>
      <webElementGuid>9db68770-1046-4695-ab1a-35c05c1b1849</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping address'])[1]/preceding::div[1]</value>
      <webElementGuid>7724f3e1-ba19-443e-b1b8-6f73f26d31fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div[2]/div</value>
      <webElementGuid>017560bf-a193-462e-8270-195697761664</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'billing-buttons-container' and (text() = '
                        
                        Loading next step...
                    ' or . = '
                        
                        Loading next step...
                    ')]</value>
      <webElementGuid>8f83eeac-63bb-4f71-84da-c6a8a35a3748</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
