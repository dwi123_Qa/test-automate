<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_alamat</name>
   <tag></tag>
   <elementGuidId>bc2ba965-b881-4b81-bc57-2d1de4c53aaa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#checkout-step-confirm-order</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-step-confirm-order']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>bf062d72-d18f-42c3-ad29-f04335abed4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkout-step-confirm-order</value>
      <webElementGuid>ec17421c-37f6-49ea-83ad-bd07810f3f44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>step a-item</value>
      <webElementGuid>4ec24c5a-872a-4bd4-be60-5d2c72d5625f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
    
    
        
        
    
    
    
        

    
        
        
            
                Billing Address
            
            
                eca lorent
            
            
                Email: akunnetflixxx00@gmail.com
            
                
                    Phone: 081302492455
                
                            
                    Fax: 081302492455
                
                                        
                    Jakarta Selatan
                
                            
                    Jakarta Selatan 2
                
                            
Jakarta                                            ,
                                        21211                
                            
                    Indonesia
                
                            
                        Payment Method
                
                
                        Cash On Delivery (COD)
                
        
            
                
                    Shipping Method
                
                
                    In-Store Pickup
                
            
    

        
            
                                                    
                
                
                
                
            
            
                
                                                                
                    
                        Product(s)
                    
                    
                        Price
                    
                    
                        Qty.
                    
                    
                        Total
                    
                
            
            
                    
                                                                            
                                
                            
                        
                            14.1-inch Laptop
                                                                                                            
                        
                            Price:
                            1590.00
                        
                        
                            Qty.:
                                2
                        
                        
                            Total:
                            3180.00
                        
                    
            
        
        
        
        
            
            
            
            
                
    
        
            
                
                    Sub-Total:
                
                
                    3180.00 
                
            
            
                
                    
                        Shipping:
                        
                            (In-Store Pickup)
                        
                
                
                    
                            0.00
                            
                    
                
            
                
                    
                        Payment method additional fee:
                    
                    
                        7.00
                        
                    
                
                                        
                    
                        Tax: 
                    
                    
                        0.00 
                    
                
                                                
                
                    
                        Total:
                
                
                    
                            3187.00
                    
                
            
        
    


            
        
    


        


                    
                        ConfirmOrder.init('https://demowebshop.tricentis.com/checkout/OpcConfirmOrder/', 'https://demowebshop.tricentis.com/checkout/completed/');
                    
                    
                        
                            « Back
                        
                        Submitting order information...
                    
                </value>
      <webElementGuid>9ed6671e-1f91-4b46-81d1-7c1a6e5c703b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-step-confirm-order&quot;)</value>
      <webElementGuid>1d62d38e-cad9-49ea-a5bb-44b85454bc8f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='checkout-step-confirm-order']</value>
      <webElementGuid>70d0e1ed-434c-44c2-88cf-a54c6f45d07f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='opc-confirm_order']/div[2]</value>
      <webElementGuid>a48abbd5-0493-4934-a92d-4c83b03af127</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm order'])[1]/following::div[1]</value>
      <webElementGuid>04e4b052-1ab1-481f-aecd-458a8eb7ee7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[5]/following::div[2]</value>
      <webElementGuid>1866ccdb-ce2b-46ea-8b05-17e14a613870</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/div[2]</value>
      <webElementGuid>7a6b531f-56ca-465d-9ca6-891f9245a9db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'checkout-step-confirm-order' and (text() = concat(&quot;
                    
    
    
        
        
    
    
    
        

    
        
        
            
                Billing Address
            
            
                eca lorent
            
            
                Email: akunnetflixxx00@gmail.com
            
                
                    Phone: 081302492455
                
                            
                    Fax: 081302492455
                
                                        
                    Jakarta Selatan
                
                            
                    Jakarta Selatan 2
                
                            
Jakarta                                            ,
                                        21211                
                            
                    Indonesia
                
                            
                        Payment Method
                
                
                        Cash On Delivery (COD)
                
        
            
                
                    Shipping Method
                
                
                    In-Store Pickup
                
            
    

        
            
                                                    
                
                
                
                
            
            
                
                                                                
                    
                        Product(s)
                    
                    
                        Price
                    
                    
                        Qty.
                    
                    
                        Total
                    
                
            
            
                    
                                                                            
                                
                            
                        
                            14.1-inch Laptop
                                                                                                            
                        
                            Price:
                            1590.00
                        
                        
                            Qty.:
                                2
                        
                        
                            Total:
                            3180.00
                        
                    
            
        
        
        
        
            
            
            
            
                
    
        
            
                
                    Sub-Total:
                
                
                    3180.00 
                
            
            
                
                    
                        Shipping:
                        
                            (In-Store Pickup)
                        
                
                
                    
                            0.00
                            
                    
                
            
                
                    
                        Payment method additional fee:
                    
                    
                        7.00
                        
                    
                
                                        
                    
                        Tax: 
                    
                    
                        0.00 
                    
                
                                                
                
                    
                        Total:
                
                
                    
                            3187.00
                    
                
            
        
    


            
        
    


        


                    
                        ConfirmOrder.init(&quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/OpcConfirmOrder/&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/completed/&quot; , &quot;'&quot; , &quot;);
                    
                    
                        
                            « Back
                        
                        Submitting order information...
                    
                &quot;) or . = concat(&quot;
                    
    
    
        
        
    
    
    
        

    
        
        
            
                Billing Address
            
            
                eca lorent
            
            
                Email: akunnetflixxx00@gmail.com
            
                
                    Phone: 081302492455
                
                            
                    Fax: 081302492455
                
                                        
                    Jakarta Selatan
                
                            
                    Jakarta Selatan 2
                
                            
Jakarta                                            ,
                                        21211                
                            
                    Indonesia
                
                            
                        Payment Method
                
                
                        Cash On Delivery (COD)
                
        
            
                
                    Shipping Method
                
                
                    In-Store Pickup
                
            
    

        
            
                                                    
                
                
                
                
            
            
                
                                                                
                    
                        Product(s)
                    
                    
                        Price
                    
                    
                        Qty.
                    
                    
                        Total
                    
                
            
            
                    
                                                                            
                                
                            
                        
                            14.1-inch Laptop
                                                                                                            
                        
                            Price:
                            1590.00
                        
                        
                            Qty.:
                                2
                        
                        
                            Total:
                            3180.00
                        
                    
            
        
        
        
        
            
            
            
            
                
    
        
            
                
                    Sub-Total:
                
                
                    3180.00 
                
            
            
                
                    
                        Shipping:
                        
                            (In-Store Pickup)
                        
                
                
                    
                            0.00
                            
                    
                
            
                
                    
                        Payment method additional fee:
                    
                    
                        7.00
                        
                    
                
                                        
                    
                        Tax: 
                    
                    
                        0.00 
                    
                
                                                
                
                    
                        Total:
                
                
                    
                            3187.00
                    
                
            
        
    


            
        
    


        


                    
                        ConfirmOrder.init(&quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/OpcConfirmOrder/&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/completed/&quot; , &quot;'&quot; , &quot;);
                    
                    
                        
                            « Back
                        
                        Submitting order information...
                    
                &quot;))]</value>
      <webElementGuid>8c4a9bf3-01e1-4f59-bc3d-77824dbd9c29</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
