<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_submit</name>
   <tag></tag>
   <elementGuidId>c70e41e8-f7ee-4571-be50-2dd8cf747ce4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#confirm-order-buttons-container</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='confirm-order-buttons-container']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>27e9de6c-851e-4f51-937c-5e07eede30df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>buttons</value>
      <webElementGuid>a788957a-96db-4fb1-bc27-045d8e662abf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>confirm-order-buttons-container</value>
      <webElementGuid>a0a80cee-ea0e-448f-afa7-0d6aaeb2ae33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            « Back
                        
                        Submitting order information...
                    </value>
      <webElementGuid>6e712b98-2f48-4b1e-9cc0-634ae8b22ab6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;confirm-order-buttons-container&quot;)</value>
      <webElementGuid>05b86792-5113-4ffe-a1b7-b16c761586a8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='confirm-order-buttons-container']</value>
      <webElementGuid>0ca6ba4b-f590-4502-b024-22e31a7eb1f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-step-confirm-order']/div[2]</value>
      <webElementGuid>3d13fded-df5d-4a04-94fc-f9c093c13187</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total:'])[2]/following::div[1]</value>
      <webElementGuid>deac0fad-485b-46df-9efa-ed3e6b718ce6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tax:'])[1]/following::div[1]</value>
      <webElementGuid>7810c15d-4a35-4fe6-95cb-549b5ca899a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/div[2]/div[2]</value>
      <webElementGuid>c0419678-1c2b-4107-8d04-98a79e524d3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'confirm-order-buttons-container' and (text() = '
                        
                            « Back
                        
                        Submitting order information...
                    ' or . = '
                        
                            « Back
                        
                        Submitting order information...
                    ')]</value>
      <webElementGuid>380fdae0-a02f-4b7a-b92e-a852871897eb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
