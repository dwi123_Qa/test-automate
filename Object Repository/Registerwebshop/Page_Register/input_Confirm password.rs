<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Confirm password</name>
   <tag></tag>
   <elementGuidId>18e587f5-a965-4498-9ab4-5cb96e215a77</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ConfirmPassword</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ConfirmPassword']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>cd8176cd-f271-44bd-bd42-e198e2169a7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line password valid</value>
      <webElementGuid>71ea9f77-43d8-48fe-935b-b2694c57166c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>9b50f281-25f8-41b5-8cdc-3998537c573d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-equalto</name>
      <type>Main</type>
      <value>The password and confirmation password do not match.</value>
      <webElementGuid>ecd2b40c-7970-4033-8d35-015091440712</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-equalto-other</name>
      <type>Main</type>
      <value>*.Password</value>
      <webElementGuid>21168e55-d09c-431c-9ef9-bb428d43edc0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>Password is required.</value>
      <webElementGuid>68301122-09f4-4069-b5c1-9f0b3bcfd53e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ConfirmPassword</value>
      <webElementGuid>de2dae0a-7915-4e1d-8ec0-f0638696c7a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ConfirmPassword</value>
      <webElementGuid>c2a51073-9fae-4d24-9a9a-c033d772430a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>b60aac55-fc67-4633-983c-967dc41a0574</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ConfirmPassword&quot;)</value>
      <webElementGuid>a9d6609d-53ac-46ea-b8bb-9ea5a53e8e79</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ConfirmPassword']</value>
      <webElementGuid>9673089a-5257-4edc-8bea-e22dde667a55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm password:'])[1]/following::input[1]</value>
      <webElementGuid>dc5a762b-3fa0-4725-adf2-145e9391d649</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/following::input[1]</value>
      <webElementGuid>47ff1159-ef12-452a-b215-58eb406e2023</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[5]/preceding::input[1]</value>
      <webElementGuid>4be340e9-e7f3-46d7-b18c-5cf671fe24a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::input[2]</value>
      <webElementGuid>a4ebcd17-9dfd-489c-bd1d-86e628afe326</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div[2]/input</value>
      <webElementGuid>2c8313d4-4a96-4844-b528-853a8c6cdf13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ConfirmPassword' and @name = 'ConfirmPassword' and @type = 'password']</value>
      <webElementGuid>e8afd527-aeff-431e-b6fb-2bd592ee659d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
