<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Search</name>
   <tag></tag>
   <elementGuidId>c21ed7a6-f05d-48b6-b5b7-7b3e1e658b63</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.center-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3469b959-6835-4320-bd0f-59697c7756a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>center-2</value>
      <webElementGuid>928e2767-fc9d-4894-b9d3-66d88adca6b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    


    $(document).ready(function () {

        $(&quot;#As&quot;).click(toggleAdvancedSearch);

        toggleAdvancedSearch();
    });

    function toggleAdvancedSearch() {

        if ($('#As').is(':checked')) {
            $('#advanced-search-block').show();
        }
        else {
            $('#advanced-search-block').hide();
        }
    }


    
        Search
    
    
        
                
                    
                        Search keyword:
                        
                    
                    
                    
                        
                        Advanced search
                    
                
                
                        
                            Category:
                            All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards

                        
                        
                            
                            Automatically search sub categories
                        
                                            
                            Manufacturer:
                            All
Tricentis

                        
                    
                        Price range:
                        
                            From
                            
                            to
                            
                        
                    
                    
                        
                        Search In product descriptions
                    
                    
                
                
                    
                
        
        
            
                
                    View as
                    Grid
List

                
                            
                    Sort by
                    Position
Name: A to Z
Name: Z to A
Price: Low to High
Price: High to Low
Created on

                
                            
                    Display
                    4
8
12

                    per page
                
        
        
                                            
                            
                                

    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    


                            
                    
            
                
            
        
    


    
</value>
      <webElementGuid>47231e73-a0f6-49a0-8cbc-a5c54ce1575a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]</value>
      <webElementGuid>7f14f8fd-7af9-477f-a06a-9be888642586</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[2]</value>
      <webElementGuid>c56f2937-73b8-4927-bef5-482b698f7e9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up for our newsletter:'])[1]/following::div[4]</value>
      <webElementGuid>4e9ed34f-2c32-4b51-9a0b-12c9701db964</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div[2]</value>
      <webElementGuid>f141c6a3-3d18-40db-9145-3f4397d0adca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
    
    


    $(document).ready(function () {

        $(&quot;#As&quot;).click(toggleAdvancedSearch);

        toggleAdvancedSearch();
    });

    function toggleAdvancedSearch() {

        if ($(&quot; , &quot;'&quot; , &quot;#As&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:checked&quot; , &quot;'&quot; , &quot;)) {
            $(&quot; , &quot;'&quot; , &quot;#advanced-search-block&quot; , &quot;'&quot; , &quot;).show();
        }
        else {
            $(&quot; , &quot;'&quot; , &quot;#advanced-search-block&quot; , &quot;'&quot; , &quot;).hide();
        }
    }


    
        Search
    
    
        
                
                    
                        Search keyword:
                        
                    
                    
                    
                        
                        Advanced search
                    
                
                
                        
                            Category:
                            All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards

                        
                        
                            
                            Automatically search sub categories
                        
                                            
                            Manufacturer:
                            All
Tricentis

                        
                    
                        Price range:
                        
                            From
                            
                            to
                            
                        
                    
                    
                        
                        Search In product descriptions
                    
                    
                
                
                    
                
        
        
            
                
                    View as
                    Grid
List

                
                            
                    Sort by
                    Position
Name: A to Z
Name: Z to A
Price: Low to High
Price: High to Low
Created on

                
                            
                    Display
                    4
8
12

                    per page
                
        
        
                                            
                            
                                

    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    


                            
                    
            
                
            
        
    


    
&quot;) or . = concat(&quot;
    
    


    $(document).ready(function () {

        $(&quot;#As&quot;).click(toggleAdvancedSearch);

        toggleAdvancedSearch();
    });

    function toggleAdvancedSearch() {

        if ($(&quot; , &quot;'&quot; , &quot;#As&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:checked&quot; , &quot;'&quot; , &quot;)) {
            $(&quot; , &quot;'&quot; , &quot;#advanced-search-block&quot; , &quot;'&quot; , &quot;).show();
        }
        else {
            $(&quot; , &quot;'&quot; , &quot;#advanced-search-block&quot; , &quot;'&quot; , &quot;).hide();
        }
    }


    
        Search
    
    
        
                
                    
                        Search keyword:
                        
                    
                    
                    
                        
                        Advanced search
                    
                
                
                        
                            Category:
                            All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards

                        
                        
                            
                            Automatically search sub categories
                        
                                            
                            Manufacturer:
                            All
Tricentis

                        
                    
                        Price range:
                        
                            From
                            
                            to
                            
                        
                    
                    
                        
                        Search In product descriptions
                    
                    
                
                
                    
                
        
        
            
                
                    View as
                    Grid
List

                
                            
                    Sort by
                    Position
Name: A to Z
Name: Z to A
Price: Low to High
Price: High to Low
Created on

                
                            
                    Display
                    4
8
12

                    per page
                
        
        
                                            
                            
                                

    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    


                            
                    
            
                
            
        
    


    
&quot;))]</value>
      <webElementGuid>c29d2b5e-937c-468c-8c80-bb6da27d037d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
