<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Laptop</name>
   <tag></tag>
   <elementGuidId>955229a1-1f77-41bd-9772-37eeb44cdbb2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.product-grid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>111d2ad6-f2d0-408f-8f46-a76c024d86f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-grid</value>
      <webElementGuid>e61491e3-f2f3-4cfc-b6de-597e63486da5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                

    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    


                            
                    </value>
      <webElementGuid>29ba1725-6e8b-4f57-8275-f6265f477356</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/div[@class=&quot;product-grid&quot;]</value>
      <webElementGuid>90e387ed-f07d-43c2-8c7a-b42f3df32ac9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[2]</value>
      <webElementGuid>f0cd15d2-0fac-4d8d-8c7b-adeea6702b55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[2]</value>
      <webElementGuid>b95baf5a-615d-4cc1-9643-73d26bef7d40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::div[13]</value>
      <webElementGuid>e6a9fb21-f44f-4c9e-a13d-d31fa974268f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div</value>
      <webElementGuid>138ce767-cce3-4ef7-8d7c-d88dee44d77c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                                

    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    


                            
                    ' or . = '
                            
                                

    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    


                            
                    ')]</value>
      <webElementGuid>18e1da77-6b65-4c9a-9c9c-86be5a1189e3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
