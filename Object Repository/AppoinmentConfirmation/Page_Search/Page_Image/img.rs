<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img</name>
   <tag></tag>
   <elementGuidId>54c087cf-10bb-48af-a41a-487e02098740</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>img[alt=&quot;Picture of 14.1-inch Laptop&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//img[@alt='Picture of 14.1-inch Laptop']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>db7b284e-9a49-4a1e-b654-9e423563dccc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Picture of 14.1-inch Laptop</value>
      <webElementGuid>27416843-c6d0-4de7-ae71-63c1a2e5893c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://demowebshop.tricentis.com/content/images/thumbs/0000224_141-inch-laptop_125.png</value>
      <webElementGuid>f5d3d7ec-a505-4c40-8650-605eb69199a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Show details for 14.1-inch Laptop</value>
      <webElementGuid>f1efb61f-1d53-4d68-9d89-9004894d602e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]/div[@class=&quot;picture&quot;]/a[1]/img[1]</value>
      <webElementGuid>a28040c0-4278-4e29-bd7d-219a4d77e5d0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='Picture of 14.1-inch Laptop']</value>
      <webElementGuid>2496668e-75d5-4097-a993-f7c1607e6749</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/a/img</value>
      <webElementGuid>457561a2-68b1-4f2f-a8fd-1960819aba68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@alt = 'Picture of 14.1-inch Laptop' and @src = 'https://demowebshop.tricentis.com/content/images/thumbs/0000224_141-inch-laptop_125.png' and @title = 'Show details for 14.1-inch Laptop']</value>
      <webElementGuid>8089806f-df10-4b44-833f-38fe94fab835</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
