<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Forgot password</name>
   <tag></tag>
   <elementGuidId>93b84ee5-c1d6-4404-a88b-979afe490233</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.forgot-password</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Remember me?'])[1]/following::span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4c64e4df-4c11-4557-b788-fdf454c59ba6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>forgot-password</value>
      <webElementGuid>34f4b6dc-6c7c-4e65-8052-4c02249b58bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                Forgot password?
                            </value>
      <webElementGuid>079257c5-33c0-4a12-900d-f37d71e0a0a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page login-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;customer-blocks&quot;]/div[@class=&quot;returning-wrapper&quot;]/div[@class=&quot;form-fields&quot;]/form[1]/div[@class=&quot;inputs reversed&quot;]/span[@class=&quot;forgot-password&quot;]</value>
      <webElementGuid>4c295c19-0337-4d7c-a2f7-8c663ccd6c1b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remember me?'])[1]/following::span[1]</value>
      <webElementGuid>0d74158c-8715-4b51-9b7b-8c56ae87ea72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password:'])[1]/following::span[2]</value>
      <webElementGuid>5c5a3e2b-6c04-493f-a158-8e03d97ce68f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About login / registration'])[1]/preceding::span[1]</value>
      <webElementGuid>e2f90eca-f78a-4d19-aeca-5285484b7102</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/span</value>
      <webElementGuid>1727dbc9-e621-4554-b42b-4b2f46458250</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
                                Forgot password?
                            ' or . = '
                                Forgot password?
                            ')]</value>
      <webElementGuid>abd0e0c6-7540-462b-9e73-58da7a699632</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
