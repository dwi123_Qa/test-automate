<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_custom</name>
   <tag></tag>
   <elementGuidId>273e5fce-3fc2-4baf-832c-8857a706f6ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.returning-wrapper</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='New Customer'])[1]/following::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>55b7a6a5-1480-49ed-bef0-44e347b2a189</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>returning-wrapper</value>
      <webElementGuid>624c074b-d24c-433a-8a2e-9d5392536a0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    Returning Customer
                
                
                        
                            
                        
                        
                            Email:
                            
                            
                        
                        
                            Password:
                            
                            
                        
                        
                            
                            Remember me?
                            
                                Forgot password?
                            
                        
                        
                            
                        
                
            </value>
      <webElementGuid>b419632b-ff6a-4d9a-9446-bbc0c9eb75d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page login-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;customer-blocks&quot;]/div[@class=&quot;returning-wrapper&quot;]</value>
      <webElementGuid>925e80c8-9c31-4456-b163-5b51eadb18db</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Customer'])[1]/following::div[3]</value>
      <webElementGuid>abbf667b-3b40-4ffe-a241-297642bf7d15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome, Please Sign In!'])[1]/following::div[7]</value>
      <webElementGuid>149e59b9-8eae-45d2-9ade-4641130ccb0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div[2]</value>
      <webElementGuid>cded16a1-6ec7-4b38-926e-7e13c82705af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                
                    Returning Customer
                
                
                        
                            
                        
                        
                            Email:
                            
                            
                        
                        
                            Password:
                            
                            
                        
                        
                            
                            Remember me?
                            
                                Forgot password?
                            
                        
                        
                            
                        
                
            ' or . = '
                
                    Returning Customer
                
                
                        
                            
                        
                        
                            Email:
                            
                            
                        
                        
                            Password:
                            
                            
                        
                        
                            
                            Remember me?
                            
                                Forgot password?
                            
                        
                        
                            
                        
                
            ')]</value>
      <webElementGuid>faffc6fe-f246-4293-b7b2-a25ec8cf236c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
